/**
 * Function does the sorting in ascending order of the array
 * @param {Array} arr 
 * @param {String} property 
 */
function sortAscending(arr, property) {
  arr.sort((a, b) => {
    if (a[property] < b[property])
      return -1;
    if (a[property] > b[property])
      return 1;
    return 0;
  });
}

/**
 * isEmpty check for the val is empty or not
 * @param {*} obj 
 * 
 * @return {Boolean}
 */
function isEmpty(val) {
  return (
    val === null ||
    val === undefined ||
    (val.hasOwnProperty('length') && val.length === 0) ||
    (val.constructor === Object && Object.keys(val).length === 0)
  )
}

module.exports = {
  sortAscending,
  isEmpty
}