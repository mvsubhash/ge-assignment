import React from 'react';

import { PlayersList, Team, PlayerDetail } from '../../components';
import { sortAscending } from '../../utils/util';
import './Dashboard.component.style.css';

class Dashboard extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      players: [],
      team: [],
      selectedPlayer: {},
      roleWiseList: {},
      messages: {}
    };
  }

  /**
   * This method is used to arrange players into category wise according to the roles and return the category players list
   * @param {Object} players 
   */
  setPlayersCategorywise(players) {
    let list = {};
    players && players.forEach((player) => {
      let role = player.role;
      if (!list[role]) {
        list[role] = [];
      }
      list[role].push(player);
    });
    return list;
  }

  /**
   * Used to fetch the players list
   * @param {String} url 
   */
  fetchPlayers(url) {
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
    })
      .then(res => res.json())
      .then((players) => {
        let roleWiseList = this.setPlayersCategorywise(players);
        this.setState({
          isLoaded: true,
          players: players,
          roleWiseList: roleWiseList
        });
      },
        (error) => {
          this.setState({
            isLoaded: true,
            error
          });
        });
  }

  /**
   * Function will be fired when player clicked and respect player will be pushed to the team array
   * @param {Object} player 
   */
  addPlayer(player) {
    const { roleWiseList, team } = { ...this.state };
    const role = player.role;
    if (team.length >= 11) {
      this.setState({
        messages: {
          team: {
            type: 'error',
            msg: 'Cannot select more than 11'
          }
        }
      });
    } else {
      let _roleWiseList = roleWiseList[role].filter(function (p) {
        return player.name !== p.name
      });
      roleWiseList[role] = _roleWiseList;

      this.setState({
        team: [...team, player],
        roleWiseList: { ...roleWiseList },
        selectedPlayer: player,
        messages: {
          team: {
            type: 'info',
            msg: (team.length + 1) + ' Player selected'
          }
        }
      });
    }
  }

  /**
   * To view the details of the player
   * @param {Object} player 
   */
  viewPlayer(player) {
    this.setState({
      selectedPlayer: player
    });
  }

  /**
   * Function is used to remove the player from the team.
   * @param {Object} player 
   */
  removePlayer(player) {
    let role = player.role;
    const { roleWiseList, team } = { ...this.state };

    let _team = team.filter(function (p) {
      return player.name !== p.name
    });

    roleWiseList[role].push(player);
    sortAscending(roleWiseList[role], 'name');

    this.setState({
      team: _team,
      roleWiseList: { ...roleWiseList },
      selectedPlayer: {},
      messages: {
        team: {
          type: 'info',
          msg: (team.length - 1) + ' Player selected'
        }
      }
    });
  }

  componentDidMount() {
    this.fetchPlayers('data/players.json');
  }

  render() {
    const { error, isLoaded, team, roleWiseList, selectedPlayer, messages } = this.state;

    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
      return <div>Loading...</div>;
    } else {
      return (
        <div className='dashboardContainer'>
          <div className='leftPanel'>
            <div className='players'>
              <PlayersList title='Batsmens' addPlayer={(player) => this.addPlayer(player)} players={roleWiseList.batsman} />
              <PlayersList title='Bowlers' addPlayer={(player) => this.addPlayer(player)} players={roleWiseList.bowler} />
              <PlayersList title='Keepers' addPlayer={(player) => this.addPlayer(player)} players={roleWiseList.keeper} />
              <PlayersList title='All Rounders' addPlayer={(player) => this.addPlayer(player)} players={roleWiseList.allRounder} />
            </div>
            <div className='playerDetail'>
              <PlayerDetail player={selectedPlayer} />
            </div>
          </div>

          <div className='rightPanel'>
            <Team viewPlayer={(player) => this.viewPlayer(player)} teamName='Indian Team' message={messages.team} removePlayer={(player) => this.removePlayer(player)} team={team} />
          </div>
        </div>
      )
    }
  }
}

export default Dashboard;