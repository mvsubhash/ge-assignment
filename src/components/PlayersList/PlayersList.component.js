import React from 'react';
import './PlayersList.component.style.css';

import { sortAscending } from '../../utils/util';

class PlayersList extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      searchResult: props.players,
      search: ''
    }
  }

  componentWillReceiveProps(nextProps) {
    /**
     * 1) Doing empty on the search value when the player got selected and pushed to the team
     * 2) And the newProps recevied after putting selecting the player, should rerender so we are assinging to the searchResult
     */
    if (nextProps.players.length !== this.props.players.length) {
      this.setState({
        search: '',
        searchResult: nextProps.players
      });
    }
  }

  /**
   * This method is used to filter the players based on the search string and return the array
   * 
   * @param {String} name 
   * @return {Array} 
   */
  filterPlayers(name) {
    let filteredPlayersList = this.props.players.filter(player => {
      return player.name.toLowerCase().indexOf(name.toLowerCase()) >= 0;
    });

    return filteredPlayersList;
  }

  /**
   * handleSearch function will be fired when the input value changes and calls the filteredPlayers method for the filtered list
   * and update the new filtered list to the searchResult
   * @param {Object} event 
   */
  handleSearch(event) {
    let filedName = event.target.name;
    let value = event.target.value;

    let filteredPlayers = this.filterPlayers(value);
    this.setState({
      [filedName]: value,
      searchResult: filteredPlayers
    });
  }

  render() {
    const { title, addPlayer } = { ...this.props };
    const { searchResult } = this.state;

    if (searchResult) { // sort the searchResult list in ascending order by name then render
      sortAscending(searchResult, 'name');
    }

    return (
      <div className='container'>
        <h4 className='playerListTitle'>{title}</h4>
        <input className='formField' type='text' placeholder='Search for player by name' name='search' onChange={(event) => this.handleSearch(event)} value={this.state.search} />
        <div className='listContainer'>
          <ul>
            {
              searchResult && searchResult.map((player, index) => {
                return <li onClick={() => addPlayer(player)} key={index}>{player.name}</li>
              })
            }
          </ul>
        </div>
      </div >
    )
  }
}

export default PlayersList;