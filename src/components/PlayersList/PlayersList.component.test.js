import React from 'react';

import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import PlayersList from './PlayersList.component';

configure({ adapter: new Adapter() });

describe('<PlayersList /> test cases', () => {
  let wrapper;
  let batsmanList = [
    {
      "id": 1,
      "name": "Rohit Sharma",
      "age": "32",
      "dob": "30-APR-1987",
      "battingStyle": "Right Handed Bat",
      "bowlingStyle": "Right-arm offbreak",
      "role": "batsman"
    },
    {
      "id": 4,
      "name": "Suresh Raina",
      "age": "32",
      "dob": "27-NOV-1986",
      "battingStyle": "Left Handed Bat",
      "bowlingStyle": "Right-arm offbreak",
      "role": "batsman"
    },
    {
      "id": 5,
      "name": "Shubman Gill",
      "age": "19",
      "dob": "08-SEP-1999",
      "battingStyle": "Right Handed Bat",
      "bowlingStyle": "Right-arm offbreak",
      "role": "batsman"
    }
  ]

  beforeEach(() => {
    wrapper = shallow(<PlayersList title='Batsmans' players={batsmanList} />);
  });

  it('should render the list of players', () => {
    wrapper.setProps({ addPlayer: () => { } });
    expect(wrapper.find('h4').text()).toEqual('Batsmans');
    expect(wrapper.find('li')).toHaveLength(3);
  });
  
  it('search filter test case', () => {
    wrapper.find('input').simulate('change', { target: { value: 'Ro'}});
    expect(wrapper.find('li')).toHaveLength(1);
  });
});