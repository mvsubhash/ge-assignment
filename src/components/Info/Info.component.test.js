import React from 'react';

import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import Info from './Info.component';

configure({ adapter: new Adapter() });

describe('<Info /> test cases', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(<Info label='testlabel' />);
  });

  it('should render two spans when label and value props are passed', () => {
    wrapper.setProps({ value: 'testvalue' });
    expect(wrapper.find('span')).toHaveLength(2);
  });

  it('should render empty span when value is not passed', () => {
    expect(wrapper.find('span')).toHaveLength(2);
  });

  it('should have default horizontal class if we are not passing layout prop', () => {
    expect(wrapper.find('div').hasClass('horizontal')).toEqual(true);
  });

  it('should have vertical class if we are passing layout prop as vertical', () => {
    wrapper.setProps({ layout: 'vertical' });
    expect(wrapper.find('div').hasClass('vertical')).toEqual(true);
  });
});