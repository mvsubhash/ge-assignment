import React from 'react';
import './Info.component.style.css';

const Info = (props) => {
  const { layout, label, value } = { ...props };

  return (
    <div className={layout ? layout : 'horizontal'}>
      <span className='infoLabel'>{label}</span>
      <span className='infoValue'>{value}</span>
    </div>
  )
}

export default Info;