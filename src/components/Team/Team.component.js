import React from 'react';
import './Team.component.style.css';

const Team = (props) => {
  const { teamName, team, removePlayer, viewPlayer, message } = { ...props };

  return (
    <div className='team-container'>
      <h4 className='teamName'>{teamName}</h4>
      <span className={message && message.type === 'error' ? 'errorText' : 'infoText'}>{message && message.msg}</span>
      <ul>
        {
          team && team.map((player, index) => {
            return (
              <li key={index}>
                <div className='row'>
                  <span>{player.name}</span>
                  <div className='row'>
                    <div onClick={() => viewPlayer(player)} className='icon'>View</div>
                    <div onClick={() => removePlayer(player)} className='icon'>Remove</div>
                  </div>
                </div>
              </li>
            )
          })
        }
      </ul>
    </div>
  )
}

export default Team;