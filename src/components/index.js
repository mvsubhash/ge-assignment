import PlayersList from './PlayersList/PlayersList.component';
import Team from './Team/Team.component';
import PlayerDetail from './PlayerDetail/PlayerDetail.component';

export {
  Team,
  PlayersList,
  PlayerDetail
}