import React from 'react';

import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import PlayerDetail from './PlayerDetail.component';

configure({adapter: new Adapter()});

describe('<PlayerDetail /> test cases', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(<PlayerDetail />);
  })

  it ('should not contain Info component when player props is empty', () => {
    expect(wrapper.find('Info')).toHaveLength(0);
    expect(wrapper.find('div')).toHaveLength(1);
    expect(wrapper.find('h4')).toHaveLength(1);
    expect(wrapper.find('h4').text()).toEqual('Select the player to see the player details');
  });

  it ('should show the player details when passed player props', () => {
    const player = {
      name: 'testplayer',
      age: 26,
      dob: '29-DEC-2019',
      battingStyle: 'Right Handed Bat',
      bowlingStyle: 'Right arm medium',
      role: 'batsman'
    }
    wrapper.setProps({ player: player });
    expect(wrapper.find('Info')).toHaveLength(6);
  });
});