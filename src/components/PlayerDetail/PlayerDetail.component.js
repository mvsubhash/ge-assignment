import React from 'react';
import Info from './../Info/Info.component';
import './PlayerDetail.component.style.css';
import { isEmpty } from '../../utils/util';

const PlayerDetail = (props) => {
  const player = props.player;

  if (isEmpty(player)) {
    return (
      <div className='playerDetailContainer'>
        <h4>Select the player to see the player details</h4>
      </div>
    );
  } else {
    return (
      <div className='playerDetailContainer'>
        <h4>Player Details</h4>
        <div className='layout-2-col'>
          <Info label='Name' value={player.name} layout='vertical' />
          <Info label='Age' value={player.age} layout='vertical' />
        </div>
        <div className='layout-2-col'>
          <Info label='DOB' value={player.dob} layout='vertical' />
          <Info label='Batting Style' value={player.battingStyle} layout='vertical' />
        </div>
        <div className='layout-2-col'>
          <Info label='Bowling Style' value={player.bowlingStyle} layout='vertical' />
          <Info label='Role' value={player.role} layout='vertical' />
        </div>
      </div >
    )
  }
}

export default PlayerDetail;